import React from 'react';
import './style.css';

const Header = ({ title }) => (
  <header className="header">
    <h2 className="header-title">
      {title}{' '}
      <span role="emoji" aria-label="sushi">
        🍣
      </span>{' '}
    </h2>
  </header>
);

export default Header;
