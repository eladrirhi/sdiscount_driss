import React, { Component } from 'react';
import Product from './../Product';
import products from './products';

class Products extends Component {
  renderProducts() {
    return products.products.map(product => <Product {...product} addProduct={this.props.addProduct} />);
  }

  render() {
    return <div className="products">{this.renderProducts()}</div>;
  }
}

export default Products;
