import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './style.css';

class Product extends Component {
  render() {
    console.log(this.props.toto);
    const { name, text, product_picture_url, addProduct, price } = this.props;
    return (
      <div className="product">
        <div className="product-picture">
          <img src={product_picture_url} alt="Avatar" />
        </div>
        <h4 className="product-name">
          {name} : {price}$
        </h4>
        <div className="product-text">{text}</div>
        <button className="product-add-button" onClick={addProduct}>
          Add
        </button>
      </div>
    );
  }
}

Product.defaultProps = {
  toto: '',
  url: null,
  price: 0,
  text: '',
};

Product.propTypes = {
  toto: PropTypes.string,
  url: PropTypes.string,
  price: PropTypes.number,
  text: PropTypes.string,
  actions: PropTypes.arrayOf(
    PropTypes.shape({
      action: PropTypes.func,
      title: PropTypes.string,
    }),
  ),
};

export default Product;
