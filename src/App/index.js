import React, { Component } from 'react';
import Header from '../Header';
import Products from './../Products';

class App extends Component {
  constructor() {
    super();
    this.addProduct = this.addProduct.bind(this);
  }

  addProduct() {
    alert('hello');
  }

  render() {
    return (
      <div className="App">
        <Header title="Sdiscount" />
        <Products addProduct={this.addProduct} />
      </div>
    );
  }
}

export default App;
